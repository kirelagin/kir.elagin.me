<!--
SPDX-FileCopyrightText: 2021 Kirill Elagin <https://kir.elagin.me/>

SPDX-License-Identifier: CC-BY-SA-4.0
-->

This is the source of the [kir.elagin.me](https://kir.elagin.me/) website.

## Usage

### Development build

* Install [Stack]
* Run `stack build` to compile the generator
* Run `stack exec -- site build` to generate the website
* The output will be in `_site/` or use `stack exec -- site watch`

### Release build

* Install [Nix]
* Compile the generator: `nix build` (the output will be in `result/`)
* Generate the website: `nix run . -- build` (into `_site/`)

[Stack]: https://www.haskellstack.org
[Nix]: https://nixos.org/guides/install-nix.html


## Colophon

* I generate the static website using [Hakyll][hakyll]
* and host it for free on [Firebase][firebase].
* I used [HTML5 Boilerplate][h5bp] as a starting point.
* The beautiful icons come from [Font Awesome][fa].

[hakyll]: https://jaspervdj.be/hakyll/
[firebase]: https://firebase.google.com
[h5bp]: https://html5boilerplate.com
[fa]: https://fontawesome.io


## Licence

![Creative Commons Attribution-ShareAlike 4.0 International License](https://licensebuttons.net/l/by-sa/4.0/80x15.png)
This work is licensed under a [Creative Commons Attribution-ShareAlike 4.0 International License](https://creativecommons.org/licenses/by-sa/4.0/).

![Creative Commons Zero 1.0](https://licensebuttons.net/p/zero/1.0/80x15.png)
Additionally, to the extent possible under law, I have waived all copyright
and related or neighboring rights to the code.
