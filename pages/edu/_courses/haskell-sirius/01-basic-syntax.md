---
title: Basic syntax
---


* [Lecture notes](01-basic-syntax.org)
* [Homework](hw1.hs)
* Reading
    * [Structure and Interpretation of Computer Programs](https://mitpress.mit.edu/sites/default/files/sicp/index.html)
    * [Learn You a Haskell for Great Good!](http://learnyouahaskell.com/)
    * [Real World Haskell](http://book.realworldhaskell.org/)
