--------------------------------------------------------------------------------

{-# OPTIONS_GHC -Wall -Wcompat
                -fno-warn-type-defaults -fno-warn-missing-signatures #-}

import Control.Exception (Exception, catch, throw)

--------------------------------------------------------------------------------

--
-- Intro:
--
-- В этом файлике в некоторые местах написаны вопросики (??). Ваша задача —
-- заменить их на что-то разумное.
--
-- Этот файл можно запустить с помощью `runhaskell`. Если вы сделаете это
-- сразу же, ничего в нем не меняя, то вы увидите две вещи:
--
-- 1. Несколько warning'ов. Они связаны с тем, что что-то нереализовано,
--    что-то где-то не используется и т.п.
--    После того, как вы всё реализуете, никаких warning’ов быть не должно.
-- 2. Результат запуска встроенных тестов ваших реализаций.
--    У каждого теста будет OK, FAIL или skipped (если вы не реализовали
--    соответствующую функцию).


--------------------------------------------------------------------------------

-- Разминка. Реализуйте функцию, умножающую число на три.

mult3 :: Integer -> Integer
mult3 x = (??)

tests_mult3 =
  [ mult3 5 == 15
  , mult3 1 == 3
  , mult3 0 == 0
  ]


-- Реализуйте функцию, которая берет функцию и меняет ей местами параметры.

swapParams = (??)

tests_swapParams =
  [ (swapParams div) 5 16 == 3
  , (swapParams replicate) 'a' 4 == "aaaa"
  ]

-- Теперь найдите эту функцию в стандартной библиотеке.


-- Реализуйте (руками, не используя ничего из библиотек) композицию функций.

compose :: (a -> b) -> (b -> c) -> (a -> c)
compose = (??)

tests_compose =
  [ compose (+2) (*5) 4 == 30
  , compose (filter odd) (map (*2)) [1 .. 6] == [2, 6, 10]
  ]


-- Реализуйте функцию:

nTimes :: Integer -> (a -> a) -> a -> a
nTimes = (??)

tests_nTimes =
  [ nTimes 3 (+1) 4 == 7
  , nTimes 15211 not True == False
  ]


-- Реализуйте функцию, которая берет функцию и оборачивает её таким
-- образом, чтобы возвращался модуль результата исходной функции.

absolutised :: (a -> Integer) -> (a -> Integer)
absolutised = (??)

tests_absolutised =
  [ (absolutised (+1))   5  == 6
  , (absolutised (+1)) (-5) == 4
  ]


-- Неподвижные точки
---------------------

-- Точность для всех последующих вычислений:

eps :: Double
eps = 0.0001

-- Сравнение двух даблов с точностью eps:

(~=) :: Double -> Double -> Bool
x ~= y = abs (x - y) <= eps
infix 4 ~=


-- Точка `x` называется неподвижной для функции `f`, если `f(x) = x`.
-- Вот функция, которая ищет неподвижную точку довольно нелепым способом:

fixedPoint
  :: (Double -> Double)  -- ^ Функция `f`
  -> Double              -- ^ Стартовая точка поиска
  -> Double
fixedPoint f start
  | f start ~= start = start
  | otherwise        = fixedPoint f (f start)


-- Теперь посчитаем квадратный корень.
-- x^2 == y  <=>  x == y / x  <=>  x — неподвижная точка функции (\x -> y / x)
--            ^ (при x /= 0)

sqrtDumb :: Double -> Double
sqrtDumb y = fixedPoint (\x -> y / x) 1

-- Это довольно глупый способ, и он почти никогда не сходится ¯\_(ツ)_/¯.


-- Но можно схитрить и заменить функцию `f` на функцию `(\x -> average x (f x)`,
-- где `average` это арифметическое среднее. При последовтельном применении,
-- которое делает `fixedPoint`, значения новой функции меняются более плавно,
-- чем `f`. Но обратите внимание на то, что если `x == f x`, то они оба также
-- равны их среднему, а потому точка является неподвижной для нашей новой функции
-- тогда и только тогда, когда была неподвижной для исходной.

-- Эта функция берет `f` и возвращает её «сплавленную» указанным выше способом версию.

averageDamp :: (Double -> Double) -> (Double -> Double)
averageDamp f = (??)
  where
    average x y = (x + y) / 2

tests_averageDamp =
  [ dampedMul2 1      ~= 1.5
  , dampedMul2 4      ~= 6
  , dampedMul2 0      ~= 0
  , dampedMul2 123456 ~= 185184

  , dampedSqr 1 ~= 1
  , dampedSqr 2 ~= 3
  , dampedSqr 5 ~= 15
  ]
 where
  dampedMul2 = averageDamp (* 2)
  dampedSqr  = averageDamp (^ 2)


-- Используйте `fixedPoint` и `averageDamp`, чтобы найти квадратный корень.

sqrtDamped :: Double -> Double
sqrtDamped y = (??)

tests_sqrtDamped = map test [1 .. 100]
 where
  test y = abs (sqrtDamped y - sqrt y) <= 0.0001


-- Метод Ньютона (посмотрите на Википедии) позволяет находить корень функции,
-- то есть такой `x`, что `f(x) = 0`. Сформулировать его можно следующим образом:
--
-- 1. Определим новую функцию `g = \x -> x - (f x) / (f' x)`, где `f'` — производная `f`.
-- 2. Найдем неподвижную точку функции `g`. Это и есть наш ответ.
--    (Из определеня g видно, что `g x == x  <=>  f x == 0).
--                            (при f' x /= 0) ^

-- Численное взятие производной я за вас уже написал:

deriv :: (Double -> Double) -> (Double -> Double)
deriv f x = (f (x + eps) - f x) / eps

-- Вы теперь используйте `fixedPoint`, чтобы методом Ньютона посчитать
-- приближение квадратного корня.
-- Подсказка:  x^2 == y  <=>  (x^2 - y) == 0  <=>  x — корень этой функции.

sqrtNewton :: Double -> Double
sqrtNewton y = (??)

tests_sqrtNewton = map test [1 .. 100]
 where
  test y = abs (sqrtNewton y - sqrt y) <= 0.0001

-- Ну и последнее. Посчитайте квадратный корень методом Ньютона ещё и смягчив
-- функцию, неподвижную точку которой ищете, с помощью `averageDamp`.
-- (Вообще, эта функция работает хуже, но почему бы её не реализовать всё равно?)

sqrtNewtonDamped :: Double -> Double
sqrtNewtonDamped y = (??)

tests_sqrtNewtonDamped = map test [1 .. 100]
 where
  test y = abs (sqrtNewtonDamped y - sqrt y) <= 0.001



------------------------------------ ~fin~ -------------------------------------


{- сюда смотреть не надо -}


data Skipped = Skipped
instance Exception Skipped where
instance Show Skipped where
  show Skipped = "skipped"

(??) = throw Skipped

ok_if_defined x = seq x True

main :: IO ()
main = sequence_ $ map (uncurry runTests) tests
  where
    tests =
      [ ("mult3", tests_mult3)
      , ("swapParams", tests_swapParams)
      , ("compose", tests_compose)
      , ("absolutised", tests_absolutised)
      , ("nTimes", tests_nTimes)

      , ("averageDamp", tests_averageDamp)
      , ("sqrtDamped", tests_sqrtDamped)
      , ("sqrtNewton", tests_sqrtNewton)
      , ("sqrtNewtonDamped", tests_sqrtNewtonDamped)
      ]

    runTests :: String -> [Bool] -> IO ()
    runTests n ts = do
      putStr $ "Running tests " ++ show n ++ "... "
      catch (putStr $ if (and ts) then "OK" else "FAIL") (\e -> putStr . show $ (e :: Skipped))
      putStrLn ""
