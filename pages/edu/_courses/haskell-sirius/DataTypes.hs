--------------------------
---- Algebraic data types
--------------------------


--
-- Sum types, pattern matching
--

data Colour = Black | Blue | Green | Red | White
  deriving (Eq, Show)

isBlack :: Colour -> Bool
isBlack Black = True
isBlack _     = False


{-
data MyBool = MyFalse | MyTrue
  deriving (Eq, Show)  -- magic

myIf :: MyBool -> a -> a -> a
myIf MyTrue  x _ = x
myIf MyFalse _ y = y

-- lazy!
-}


--
-- Product types
--

data CatInfo = CatInfo String Colour
  deriving (Eq, Show)

catName :: CatInfo -> String
catName (CatInfo name _) = name

catColour :: CatInfo -> Colour
catColour (CatInfo _ colour) = colour


--
-- Inductive types
--

data Nat = Zero | Succ Nat
  deriving (Eq, Show)

fromInt :: Int -> Nat
fromInt 0 = Zero
fromInt n = Succ $ fromInt (n - 1)

toInt :: Nat -> Int
toInt Zero = 0
toInt (Succ n) = 1 + toInt n

plus :: Nat -> Nat -> Nat
plus Zero m = m
plus (Succ n) m = Succ (plus n m)

plus' :: Nat -> Nat -> Nat
plus' Zero m = m
plus' (Succ n) m = plus' n (Succ m)

--
-- Type parameters
--

data PairIntString = PairIS Int String  -- inconvenient

-- Pair = \a b -> Pair a b
data Pair a b = Pair a b
  deriving (Eq, Show)

pairCurry :: (Pair a b -> c) -> (a -> b -> c)
pairCurry f = \a b -> f (Pair a b)

pairUncurry :: (a -> b -> c) -> (Pair a b -> c)
pairUncurry f = \(Pair a b) -> f a b


--
-- List
--

data List a = Nil | Cons a (List a)
  deriving (Eq, Show)

{- Built-in list:
data [a] = [] | a : [a]
-}

isEmpty :: List a -> Bool
isEmpty Nil = True
isEmpty _   = False

listLength :: List a -> Nat
listLength Nil = Zero
listLength (Cons _ xs) = Succ $ listLength xs

listMap :: (a -> b) -> List a -> List b
listMap _ Nil = Nil
listMap f (Cons x xs) = Cons (f x) (listMap f xs)

listFilter :: (a -> Bool) -> List a -> List a
listFilter _ Nil = Nil
listFilter p (Cons x xs)
    | p x = Cons x (listFilter p xs)
    | otherwise = listFilter p xs

partition :: (a -> Bool) -> [a] -> ([a], [a])
partition _ [] = ([], [])
partition p (x : xs) =
  case partition p xs of
    (part1, part2) ->
      if p x then (x : part1, part2) else (part1, x : part2)

dumbSort :: [Int] -> [Int]
dumbSort [] = []
dumbSort (x : xs) = dumbSort smaller ++ [x] ++ dumbSort bigger
  where
    (smaller, bigger) = partition (<= x) xs

{-
listFind :: (a -> Bool) -> List a -> ???
-}


--
-- Maybe
--

data MyMaybe a = MyNothing | MyJust a
  deriving (Eq, Show)

listFind :: (a -> Bool) -> List a -> MyMaybe a
listFind _ Nil = MyNothing
listFind p (Cons x xs)
    | p x = MyJust x
    | otherwise = listFind p xs

isMyJust :: MyMaybe a -> Bool
isMyJust (MyJust _) = True
isMyJust _ = False

mFilter :: List (MyMaybe a) -> List a
mFilter = listMap (\(MyJust a) -> a) . listFilter isMyJust  -- bad


--
-- Lambda terms
--

type Variable = String

data Term
  = Var Variable
  | Lam Variable Term
  | Term :@: Term

instance Show Term where
  show (Var v) = v
  show (Lam v t) = "(\\" ++ v ++ "." ++ show t ++ ")"
  show (f :@: x) = "(" ++ show f ++ " " ++ show x ++ ")"

free :: Term -> [Variable]
free (Var v) = [v]
free (Lam v t) = filter (/= v) . free $ t
free (f :@: x) = free f ++ free x

bound :: Term -> [Variable]
bound (Var _) = []
bound (Lam v t) = v : bound t
bound (f :@: x) = bound f ++ bound x

isBetaRedex :: Term -> Bool
isBetaRedex ((Lam _ _) :@: _) = True
isBetaRedex _ = False

isNormalForm :: Term -> Bool
isNormalForm (Var _) = True
isNormalForm (Lam _ t) = isNormalForm t
isNormalForm t@(f :@: x)
  = not (isBetaRedex t)
 && isNormalForm f
 && isNormalForm x
