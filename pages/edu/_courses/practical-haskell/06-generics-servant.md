---
title: Generics and servant
---


Generics
---------

* [`Generic`], [`DeriveGeneric`]
* [`Rep`], [`from`], [`to`]
* [`generics-sop`]
* [`DefaultSignatures`]
* [`DeriveAnyClass`] and issues with [`GeneralizedNewtypeDeriving`]
* [`DerivingVia`]
* [`DerivingStrategies`]


[`servant`]
------------

* API as a type
* [`Capture`], [`ReqBody`]
* API server implementation
* [`wai`], [`warp`]
* API client generation
* [`servant-generic`]


[`DefaultSignatures`]: https://downloads.haskell.org/~ghc/latest/docs/html/users_guide/glasgow_exts.html#extension-DefaultSignatures
[`DeriveGeneric`]: https://downloads.haskell.org/~ghc/latest/docs/html/users_guide/glasgow_exts.html#extension-DeriveGeneric
[`DeriveAnyClass`]: https://downloads.haskell.org/~ghc/latest/docs/html/users_guide/glasgow_exts.html#extension-DeriveAnyClass
[`DerivingVia`]: https://downloads.haskell.org/~ghc/latest/docs/html/users_guide/glasgow_exts.html#extension-DerivingVia
[`DerivingStrategies`]: https://downloads.haskell.org/~ghc/latest/docs/html/users_guide/glasgow_exts.html#extension-DerivingStrategies
[`GeneralizedNewtypeDeriving`]: https://downloads.haskell.org/~ghc/latest/docs/html/users_guide/glasgow_exts.html#extension-GeneralizedNewtypeDeriving

[`generics-sop`]: https://hackage.haskell.org/package/generics-sop
[`servant`]: https://hackage.haskell.org/package/servant
[`wai`]: https://hackage.haskell.org/package/wai
[`warp`]: https://hackage.haskell.org/package/warp
[`servant-generic`]: https://hackage.haskell.org/package/servant-generic

[`Generic`]: https://hackage.haskell.org/package/base/docs/GHC-Generics.html#t:Generic
[`Rep`]: https://hackage.haskell.org/package/base/docs/GHC-Generics.html#t:Rep
[`from`]: https://hackage.haskell.org/package/base/docs/GHC-Generics.html#v:from
[`to`]: https://hackage.haskell.org/package/base/docs/GHC-Generics.html#v:to
[`Capture`]: https://hackage.haskell.org/package/servant/docs/Servant-API.html#t:Capture
[`ReqBody`]: https://hackage.haskell.org/package/servant/docs/Servant-API.html#t:ReqBody
