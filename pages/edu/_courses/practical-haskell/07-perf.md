---
title: Performance and profiling
---

* [`-prof`], [`-fprof-auto`]
* [`-rtsopts`], RTS: [`-s`], [`-p`], [`-hc`], [`-hy`], [`-hd`]
* [`{-# SCC #-}`][SCC], [`{-# INLINE #-}`][INLINE], [`{-# UNPACK #-}`][UNPACK]
* [`ByteString`], [lazy `ByteString`], [`Text`], [lazy `Text`]
* [`async`]
* [`streamly`]


[`-prof`]: https://downloads.haskell.org/~ghc/latest/docs/html/users_guide/profiling.html#ghc-flag--prof
[`-fprof-auto`]: https://downloads.haskell.org/~ghc/latest/docs/html/users_guide/profiling.html#ghc-flag--fprof-auto
[`-rtsopts`]: https://downloads.haskell.org/~ghc/latest/docs/html/users_guide/phases.html#ghc-flag--rtsopts[=%E2%9F%A8none|some|all|ignore|ignoreAll%E2%9F%A9]
[`-s`]: https://downloads.haskell.org/~ghc/latest/docs/html/users_guide/runtime_control.html#rts-flag--s%20[%E2%9F%A8file%E2%9F%A9]
[`-p`]: https://downloads.haskell.org/~ghc/latest/docs/html/users_guide/profiling.html#rts-flag--p
[`-hc`]: https://downloads.haskell.org/~ghc/latest/docs/html/users_guide/profiling.html#rts-flag--hc
[`-hy`]: https://downloads.haskell.org/~ghc/latest/docs/html/users_guide/profiling.html#rts-flag--hy
[`-hd`]: https://downloads.haskell.org/~ghc/latest/docs/html/users_guide/profiling.html#rts-flag--hd

[SCC]: https://downloads.haskell.org/~ghc/latest/docs/html/users_guide/profiling.html#inserting-cost-centres-by-hand
[INLINE]: https://downloads.haskell.org/~ghc/latest/docs/html/users_guide/glasgow_exts.html#inline-and-noinline-pragmas
[UNPACK]: https://downloads.haskell.org/~ghc/latest/docs/html/users_guide/glasgow_exts.html#unpack-pragma

[`async`]: https://hackage.haskell.org/package/async
[`streamly`]: https://hackage.haskell.org/package/streamly

[`ByteString`]: https://hackage.haskell.org/package/bytestring/docs/Data-ByteString.html#t:ByteString
[lazy `ByteString`]: https://hackage.haskell.org/package/bytestring/docs/Data-ByteString-Lazy.html#t:ByteString
[`Text`]: https://hackage.haskell.org/package/text/docs/Data-Text.html#t:Text
[lazy `Text`]: https://hackage.haskell.org/package/text/docs/Data-Text-Lazy.html#t:Text
