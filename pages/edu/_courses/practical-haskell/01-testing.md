---
title: Testing
---


* [HUnit], [hspec]
* [QuickCheck], [hedgehog], [validity]
* [tasty], [tasty-discover]

<!-- -->

* Properties
* Applicative-style generators
* Shrinking


[HUnit]: https://hackage.haskell.org/package/HUnit
[hspec]: https://hackage.haskell.org/package/hspec
[QuickCheck]: https://hackage.haskell.org/package/QuickCheck
[hedgehog]: https://hackage.haskell.org/package/hedgehog
[validity]: https://hackage.haskell.org/package/validity
[tasty]: https://hackage.haskell.org/package/tasty
[tasty-discover]: https://hackage.haskell.org/package/tasty-discover
