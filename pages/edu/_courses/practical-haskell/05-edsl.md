---
title: Finally tagless eDSL
---

* Embedded DSLs
* Tagged vs. tagless values
* [`StandaloneDeriving`]
* Equality constraints
* [`OverloadedStrings`], [`OverloadedLabels`]
* [`RebindableSyntax`]
* [Overlapping instances]
* [`PartialTypeSignatures`]
* Initial vs. “final” (initial as well!) encoding
* Final encoding for effects = [`mtl`]


[`StandaloneDeriving`]: https://downloads.haskell.org/~ghc/latest/docs/html/users_guide/glasgow_exts.html#extension-StandaloneDeriving
[`OverloadedStrings`]: https://downloads.haskell.org/~ghc/latest/docs/html/users_guide/glasgow_exts.html#extension-OverloadedStrings
[`OverloadedLabels`]: https://downloads.haskell.org/~ghc/latest/docs/html/users_guide/glasgow_exts.html#extension-OverloadedLabels
[`RebindableSyntax`]: https://downloads.haskell.org/~ghc/latest/docs/html/users_guide/glasgow_exts.html#extension-RebindableSyntax
[Overlapping instances]: https://downloads.haskell.org/~ghc/latest/docs/html/users_guide/glasgow_exts.html#instance-overlap
[`PartialTypeSignatures`]: https://downloads.haskell.org/~ghc/latest/docs/html/users_guide/glasgow_exts.html#extension-PartialTypeSignatures

[`mtl`]: https://hackage.haskell.org/package/mtl
