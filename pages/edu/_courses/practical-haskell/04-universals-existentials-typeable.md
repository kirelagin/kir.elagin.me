---
title: Universals/existentials and Typeable
---

Universal quantification
-------------------------

* Higher-rank polymorphism, [`RankNTypes`]
* (rigid, skolem) type variables ([`ST`], [`STRef`])


Existential types
------------------

* Natural syntax with GADTs
* [`ExistentialQuantification`]
* Instance dictionaries inside constructors

Typeable
---------

* [`Typeable`], [`TypeRep`] (`Type.Reflection`)
* `Variant` (aka [`Data.Dynamic`])
* Propositional equality ([`:~~:`], `HRefl`)


[`RankNTypes`]: https://downloads.haskell.org/~ghc/latest/docs/html/users_guide/glasgow_exts.html#extension-RankNTypes
[`ExistentialQuantification`]: https://downloads.haskell.org/~ghc/latest/docs/html/users_guide/glasgow_exts.html#extension-ExistentialQuantification

[`ST`]: https://hackage.haskell.org/package/base/docs/Control-Monad-ST.html#t:ST
[`STRef`]: https://hackage.haskell.org/package/base/docs/Data-STRef.html
[`Typeable`]: https://hackage.haskell.org/package/base/docs/Type-Reflection.html#t:Typeable
[`TypeRep`]: https://hackage.haskell.org/package/base/docs/Type-Reflection.html#t:TypeRep
[`Data.Dynamic`]: https://hackage.haskell.org/package/base/docs/Data-Dynamic.html
[`:~~:`]: https://hackage.haskell.org/package/base/docs/Type-Reflection.html#t::-126--126-:
