---
title: Heterogenous data structures
---

* Tuples
* [Promoted lists]
* [`HList`]
* [`Constraint`] kind
* Constraining all items in a type-list
* [`ConstraintKinds`]
* [`:kind` and `:kind!`][:kind]
* `Vec` vs. `HList`
* Heterogenous `map` function


[Promoted lists]: https://downloads.haskell.org/~ghc/latest/docs/html/users_guide/glasgow_exts.html#promoted-list-and-tuple-types
[`ConstraintKinds`]: https://downloads.haskell.org/~ghc/latest/docs/html/users_guide/glasgow_exts.html#extension-ConstraintKinds
[:kind]: https://downloads.haskell.org/~ghc/latest/docs/html/users_guide/ghci.html#ghci-cmd-:kind

[`HList`]: https://hackage.haskell.org/package/HList/docs/Data-HList-HList.html
[`Constraint`]: https://hackage.haskell.org/package/base/docs/Data-Kind.html#t:Constraint
