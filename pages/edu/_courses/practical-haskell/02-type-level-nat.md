---
title: Type-level Naturals
---


* [`GADTs`]
* [`DataKinds`], [`KindSignatures`]
* [`TypeFamilies`], [`TypeOperators`]
* `class` as a function from types to terms
* [`Proxy`]
* [`TypeApplications`]


[`GADTs`]: https://downloads.haskell.org/~ghc/latest/docs/html/users_guide/glasgow_exts.html#extension-GADTs
[`DataKinds`]: https://downloads.haskell.org/~ghc/latest/docs/html/users_guide/glasgow_exts.html#extension-DataKinds
[`KindSignatures`]: https://downloads.haskell.org/~ghc/latest/docs/html/users_guide/glasgow_exts.html#extension-KindSignatures
[`TypeFamilies`]: https://downloads.haskell.org/~ghc/latest/docs/html/users_guide/glasgow_exts.html#extension-TypeFamilies
[`TypeOperators`]: https://downloads.haskell.org/~ghc/latest/docs/html/users_guide/glasgow_exts.html#extension-TypeOperators
[`TypeApplications`]: https://downloads.haskell.org/~ghc/latest/docs/html/users_guide/glasgow_exts.html#extension-TypeApplications

[`Proxy`]: https://hackage.haskell.org/package/base/docs/Data-Proxy.html#t:Proxy
