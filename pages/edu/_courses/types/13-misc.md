---
title: Other topics
---


Zippers
--------

* One-hole contexts, zippers
* Derivative of a type


Intersection types
-------------------

Substructural type systems
---------------------------

* Structural rules
* Linear, affine, relative types
