---
title: Syntax & semantics
---


* Grammars
* Inference rules
* Inductive definitions
* Operational semantics
