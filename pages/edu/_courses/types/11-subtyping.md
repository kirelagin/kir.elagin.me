---
title: Subtyping
---


* Liskov substitution principle
* Subsumption
* Subtyping relation
* Structural versus nominal subtyping
* Record subtyping: width, depth
* Variance
* Top and bottom
* Effects of mutability
* Bounded quantification, System F-sub
