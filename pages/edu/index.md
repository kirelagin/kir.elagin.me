---
title: Educational activities
---


Teaching
=========

\$for(courses)\$<section id="$id$">
<h2>\$title\$\$if(subtitle)\$ <small>\$subtitle\$</small>\$endif\$</h2>

<ol>
\$for(lectures)\$
<li><a href="$url$">\$title\$</a></li>
\$endfor\$
</ol>
</section>\$endfor\$
