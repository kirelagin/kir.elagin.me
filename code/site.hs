-- SPDX-FileCopyrightText: 2021 Kirill Elagin <https://kir.elagin.me/>
--
-- SPDX-License-Identifier: CC0-1.0

{-# LANGUAGE OverloadedStrings #-}

import Control.Applicative (empty)
import Data.Aeson ((.:), (.:?), FromJSON (parseJSON), withObject)
import Data.Binary (Binary)
import Data.Maybe (fromMaybe)
import GHC.Generics (Generic)
import Hakyll

import Edu qualified
import Util (noExt, pageCompiler, pageBackHome, yamlCompiler)

conf :: Configuration
conf = defaultConfiguration
  { providerDirectory = "pages"
  , destinationDirectory = "_site"
  , deployCommand = "firebase deploy"
  , storeDirectory = ".cache"
  , tmpDirectory = ".cache/tmp"
  }

main :: IO ()
main = hakyllWith conf $ do

    ----
    -- Load data
    ----

    match "templates/**" $ compile templateCompiler

    match "data/profiles.yaml" $ compile (yamlCompiler @[Profile])


    ----
    -- Static
    ----

    match (fromList ["404.html", "favicon.ico", "favicon.png", "humans.txt", "robots.txt"]) $ do
      route   idRoute
      compile copyFileCompiler

    match (fromList ["cv.pdf"]) $ do
      route   idRoute
      compile copyFileCompiler

    match ("css/**" .||. "js/**" .||. "img/**") $ do
      route   idRoute
      compile compressCssCompiler


    ----
    -- Render data
    ----

    match "index.html" $ do
      route idRoute
      compile $
        getResourceBody
          >>= applyAsTemplate profilesContext
          >>= loadAndApplyTemplate "templates/default.html" defaultContext
          >>= relativizeUrls

    match "profiles.html" $ do
      route noExt
      compile $
        getResourceBody
          >>= applyAsTemplate profilesContext
          >>= pageCompiler pageBackHome

    Edu.rules


--------------------------------------------------------------------------------

data Profile = Profile
  { title :: String
  , url :: String
  , important :: Bool
  , icon :: Maybe String
  }
  deriving stock Generic
  deriving anyclass Binary

instance Writable [Profile] where
  write _ _ = pure ()

instance FromJSON Profile where
  parseJSON = withObject "Profile" $ \v -> Profile
    <$> v .: "title"
    <*> v .: "url"
    <*> (fromMaybe False <$> v .:? "important")
    <*> v .:? "icon"

profilesContext :: Context a
profilesContext = listField "profiles" profileContext (sequenceA <$> load "data/profiles.yaml")
  where
    profileContext :: Context Profile
    profileContext = mconcat
      [ field "title" (return . title . itemBody)
      , field "url" (return . url . itemBody)
      , field "icon" (maybe empty return . icon . itemBody)
      , boolField "important" (important . itemBody)
      ] <> missingField
