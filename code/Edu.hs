-- SPDX-FileCopyrightText: 2021 Kirill Elagin <https://kir.elagin.me/>
--
-- SPDX-License-Identifier: CC0-1.0

{-# LANGUAGE OverloadedStrings #-}

module Edu (rules) where

import Hakyll
import System.FilePath ((</>), takeFileName, takeDirectory)

import Util

rules :: Rules ()
rules = do
    match "edu/_courses/*/course.md" $ do
      compile pandocCompiler

    match "edu/_courses/*/*-*.md" lectureRules

    match ("edu/_courses/*/*" .&&. complement "edu/_courses/*/*.md") $ do
      route $ gsubRoute "_courses/" (const "")
      compile copyFileCompiler

    match "edu/index.md" $ do
      route (setExtension "html")
      compile $ pandocCompiler
            >>= indexCompiler
            >>= pageCompiler pageBackHome

lectureRules :: Rules ()
lectureRules = do
    route lectureRoute
    compile $ pandocCompiler
          >>= loadAndApplyTemplate "templates/edu/lecture.html" ctx
          >>= pageCompiler ctx
  where
    lectureRoute :: Routes
    lectureRoute = gsubRoute "_courses/" (const "") `composeRoutes` setExtension "html"

    ctx :: Context String
    ctx = pageBack (mapContext ("../#" ++) . courseIdField) courseTitleField
       <> defaultContext

courseIdField :: String -> Context String
courseIdField name = mapContext (takeFileName . takeDirectory) (pathField name)

courseTitleField :: String -> Context String
courseTitleField name = field name $ \item -> do
  let dir = takeDirectory . toFilePath . itemIdentifier $ item
  getMetadataField' (fromFilePath $ dir </> "course.md") "title"

indexCompiler :: Item String -> Compiler (Item String)
indexCompiler item = applyAsTemplate indexCtx item
  where
    indexCtx :: Context String
    indexCtx = listField "courses" courseCtx (loadAll "edu/_courses/*/course.md")

    courseCtx :: Context String
    courseCtx = courseIdField "id"
             <> listFieldWith "lectures" defaultContext courseLectures
             <> defaultContext

    courseLectures :: Item String -> Compiler [Item String]
    courseLectures course = do
      let dir = takeDirectory . toFilePath . itemIdentifier $ course
      loadAll . fromGlob $ dir </> "*-*.md"
