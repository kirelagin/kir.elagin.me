-- SPDX-FileCopyrightText: 2021 Kirill Elagin <https://kir.elagin.me/>
--
-- SPDX-License-Identifier: CC0-1.0

{-# LANGUAGE OverloadedStrings #-}

module Util ( noExt
            , pageCompiler
            , pageBack, pageBackHome
            , yamlCompiler
            ) where

import Control.Monad.Except (throwError)
import Data.ByteString.Lazy (toStrict)
import Data.Yaml qualified as Yaml
import Hakyll.Core.Compiler (Compiler, getResourceLBS)
import Hakyll.Core.Identifier (toFilePath)
import Hakyll.Core.Item (Item, withItemBody)
import Hakyll.Core.Routes (Routes, customRoute)
import Hakyll.Web.Html.RelativizeUrls (relativizeUrls)
import Hakyll.Web.Template (loadAndApplyTemplate)
import Hakyll.Web.Template.Context (Context, defaultContext
                                   , constField, bodyField
                                   , missingField)
import System.FilePath ( splitFileName, dropExtension
                       , (</>)
                       )

-----

noExt :: Routes
noExt = customRoute $ fixPath . toFilePath
  where fixPath p = let (dir, file) = splitFileName p
                        name = dropExtension file
                    in dir </> name </> "index.html"

-----

yamlCompiler :: Yaml.FromJSON a => Compiler (Item a)
yamlCompiler = getResourceLBS >>= withItemBody decodeYaml
  where
    decodeYaml lbs =
      case Yaml.decodeEither' (toStrict lbs) of
        Right res -> pure res
        Left parseExc -> throwError [Yaml.prettyPrintParseException parseExc]

-----

pageCompiler :: Context String -> Item String -> Compiler (Item String)
pageCompiler ctx item = return item
    >>= loadAndApplyTemplate "templates/page.html" (ctx <> pageDefaultContext)
    >>= loadAndApplyTemplate "templates/default.html" (ctx <> defaultContext)
    >>= relativizeUrls
  where
    pageDefaultContext :: Context String
    pageDefaultContext = constField "nav_back_url"   "../"
                      <> constField "nav_back_title" "back"
                      <> bodyField "body"
                      <> missingField

pageBack :: (String -> Context String) -> (String -> Context String ) -> Context String
pageBack backUrl backTitle = backUrl   "nav_back_url"
                          <> backTitle "nav_back_title"

pageBackHome :: Context String
pageBackHome = pageBack (flip constField "/") (flip constField "home")
